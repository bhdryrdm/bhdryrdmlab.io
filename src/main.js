import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './registerServiceWorker'
import VueDisqus from 'vue-disqus'
import ArgonDashboard from './plugins/argon-dashboard'

Vue.config.productionTip = false

Vue.use(ArgonDashboard)
Vue.use(VueDisqus)

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
